package it.unibo.oop.lab.reactivegui03;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.InvocationTargetException;
import java.util.concurrent.TimeUnit;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;




public class AnotherConcurrentGUI extends JFrame {

        private static final long serialVersionUID = 1L;
        private static final double WIDTH_PERC = 0.2;
        private static final double HEIGHT_PERC = 0.1;
        private final JLabel display = new JLabel();
        private final JButton stop = new JButton("stop");
        private final JButton down = new JButton("down");
        private final JButton up = new JButton("up");

        /**
         * Builds a new CGUI.
         */
        public AnotherConcurrentGUI() {
            
                super();
                final Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
                this.setSize((int) (screenSize.getWidth() * WIDTH_PERC), (int) (screenSize.getHeight() * HEIGHT_PERC));
                this.setDefaultCloseOperation(EXIT_ON_CLOSE);
                final JPanel panel = new JPanel();
                panel.add(display);
                panel.add(up);
                panel.add(down);
                panel.add(stop);
                this.getContentPane().add(panel);
                this.setVisible(true);
                /*
                 * Create the counter agent and start it. This is actually not so good:
                 * thread management should be left to
                 * java.util.concurrent.ExecutorService
                 */
                final Agent agent = new Agent();
                new Thread(agent).start();
                final Agent2 agent2 = new Agent2();
                new Thread(agent2).start();
                /*
                 * Register a listener that stops it
                 */
                stop.addActionListener(new ActionListener() {
                    /**
                     * event handler associated to action event on button stop.
                     * 
                     * @param e
                     *            the action event that will be handled by this listener
                     */
                    @Override
                    public void actionPerformed(final ActionEvent e) {
                        // Agent should be final
                        agent.stopCounting();
                    }
                });
                
                down.addActionListener(new ActionListener() {
                    
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        agent.setIncrement(false);
                    }
                });
                
                up.addActionListener(new ActionListener() {
                    
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        agent.setIncrement(true);
                    }
                });
            }

            /*
             * The counter agent is implemented as a nested class. This makes it
             * invisible outside and encapsulated.
             */
            private class Agent implements Runnable {
                /*
                 * stop is volatile to ensure ordered access
                 */
                private volatile boolean stop;
                private int counter;
                private volatile boolean increment = true;

                public void run() {
                    while (!this.stop) {
                        try {
                            /*
                             * All the operations on the GUI must be performed by the
                             * Event-Dispatch Thread (EDT)!
                             */
                            SwingUtilities.invokeAndWait(new Runnable() {
                                public void run() {
                                    AnotherConcurrentGUI.this.display.setText(Integer.toString(Agent.this.counter));
                                }
                            });
                            if(increment) {
                                this.counter++;
                            } else {
                                this.counter--;
                            }
                            Thread.sleep(100);
                        } catch (InvocationTargetException | InterruptedException ex) {
                            /*
                             * This is just a stack trace print, in a real program there
                             * should be some logging and decent error reporting
                             */
                            ex.printStackTrace();
                        }
                    }
                }

                /**
                 * External command to stop counting.
                 */
                public void stopCounting() {
                    this.stop = true;
//                    try {
//                        SwingUtilities.invokeAndWait(new Runnable() {
//                            public void run() {
                                 up.setEnabled(false);
                                 down.setEnabled(false);
                            }
                 
//                    });
//                    } catch (InvocationTargetException e) {
//                        // TODO Auto-generated catch block
//                        e.printStackTrace();
//                    } catch (InterruptedException e) {
//                        // TODO Auto-generated catch block
//                        e.printStackTrace();
//                    }
//                }
                /**
                 * External command to decrement the counter.
                 */
                public void setIncrement(final boolean inc) {
                    this.increment = inc;
                }
            }
        
        private class Agent2 implements Runnable {

            @Override
            public void run() {
                try {
                    Thread.sleep(TimeUnit.SECONDS.toMillis(10));
                    SwingUtilities.invokeAndWait(new Runnable() {
                        @Override
                        public void run() {
                            AnotherConcurrentGUI.this.stop.doClick();
                        }
                    });
                } catch (InvocationTargetException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                } catch (InterruptedException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
        }
    }


